package rizqi.khoirul.uas_121_137

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import org.json.JSONObject

class Login : AppCompatActivity(),View.OnClickListener {
    val dadmin = "http://192.168.1.20/web/Android/200518_UAS/show/data_admin.php"
    val dguru = "http://192.168.1.20/web/Android/200518_UAS/show/data_guru.php"
    val dortu = "http://192.168.1.20/web/Android/200518_UAS/show/data_ortu.php"
    val dsiswa = "http://192.168.1.20/web/Android/200518_UAS/show/data_siswa.php"
    var user = ""
    var pass = ""
    var posisi = ""
    var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        var p : Bundle?=intent.extras
        posisi = p?.getString("pos").toString()
        btnLogin.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin->{
                user=txuser.text.toString()
                pass=txpass.text.toString()
                //D()
                //dAdmin("select")
                //Toast.makeText(this,"Masuk kedalam $posisi, $user, $pass", Toast.LENGTH_SHORT).show()
                gid()
                //finish()
            }
            R.id.btnCancel->{
                id=""
                user=""
                pass=""
                posisi=""
                //Toast.makeText(this,"Tidak Masuk kedalam $posisi, $user, $pass", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    fun ok(){
        var data = Intent()
        data.putExtra("A",user)
        data.putExtra("B",pass)
        data.putExtra("C",posisi)
        data.putExtra("D",id)
        setResult(Activity.RESULT_OK,data)
        finish()
    }

    fun gid(){
        if(posisi=="Admin"){
            dAdmin("select")
        }
        if(posisi=="Guru"){
            dGuru("select")
        }
        if(posisi=="Ortu"){
            dOrtu("select")
        }
        if(posisi=="Siswa"){
            dSiswa("select")
        }
    }

    fun dAdmin(mode : String){
        //Toast.makeText(this,"Cari DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
        val request = object : StringRequest(
            Request.Method.POST,dadmin,
            Response.Listener { response ->
            //Toast.makeText(this,"DATA DIteMUKAN $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
            val jsonObject = JSONObject(response)
            val error = jsonObject.getString("kode")
            id=error
            Toast.makeText(this,"DATA ID $id",Toast.LENGTH_SHORT).show()
                ok()
            /*if(error!=="0"&&error!==null){
                id=error
                Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
            }
            else{
                //Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
            }*/
        },Response.ErrorListener { error ->
        }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "select"->{
                        hm.put("XIC",user)
                        hm.put("XIB",pass)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun dGuru(mode : String){
        //Toast.makeText(this,"Cari DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
        val request = object : StringRequest(
            Request.Method.POST,dguru,
            Response.Listener { response ->
                //Toast.makeText(this,"DATA DIteMUKAN $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                //Toast.makeText(this,"DATA ",Toast.LENGTH_SHORT).show()
                id=error
                ok()
                /*if(error!=="0"&&error!==null){
                    id=error
                    //Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                }
                else{
                    id="0"
                    //Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                }*/
            },Response.ErrorListener { error ->
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "select"->{
                        hm.put("XIC",user)
                        hm.put("XIB",pass)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun dOrtu(mode : String){
        //Toast.makeText(this,"Cari DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
        val request = object : StringRequest(
            Request.Method.POST,dortu,
            Response.Listener { response ->
                //Toast.makeText(this,"DATA DIteMUKAN $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                //Toast.makeText(this,"DATA ",Toast.LENGTH_SHORT).show()
                id=error
                Toast.makeText(this,"DATA ID $id",Toast.LENGTH_SHORT).show()
                ok()
                /*if(error!=="0"&&error!==null){
                    id=error
                    //Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                }
                else{
                    id="0"
                    //Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                }*/
            },Response.ErrorListener { error ->
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "select"->{
                        hm.put("XIC",user)
                        hm.put("XIB",pass)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun dSiswa(mode : String){
        //Toast.makeText(this,"Cari DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
        val request = object : StringRequest(
            Request.Method.POST,dsiswa,
            Response.Listener { response ->
                //Toast.makeText(this,"DATA DIteMUKAN $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                id=error
                Toast.makeText(this,"DATA ID $id",Toast.LENGTH_SHORT).show()
                ok()
                /*if(error!=="0"&&error!==null){
                    id=error
                    //Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                }
                else{
                    id="0"
                    //Toast.makeText(this,"DATA $posisi, $user, $pass, $id", Toast.LENGTH_SHORT).show()
                }*/
            },Response.ErrorListener { error ->
            }
        ){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "select"->{
                        hm.put("XIC",user)
                        hm.put("XIB",pass)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
