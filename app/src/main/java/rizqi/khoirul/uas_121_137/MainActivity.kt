package rizqi.khoirul.uas_121_137

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import rizqi.khoirul.uas_121_137.Admin.Admin
import rizqi.khoirul.uas_121_137.Guru.Guru
import rizqi.khoirul.uas_121_137.Ortu.Ortu
import rizqi.khoirul.uas_121_137.Siswa.Siswa

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val RC_LOGIN_SUKSES : Int = 10
    val RC_LOGOUT_SUKSES : Int = 20
    lateinit var SETTING : SharedPreferences
    val SNAMA = "Setting"
    val SUSER = "User"
    val SPASS = "Pass"
    val SPOS = "Pos"
    val SID = "Id"
    var user = ""
    var pass = ""
    var posisi = ""
    var id = ""
    val IUSER = user
    val IPASS = pass
    val IPOSISI = posisi
    val IID = id


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        SETTING = getSharedPreferences(SNAMA, Context.MODE_PRIVATE)
        user = SETTING.getString(SUSER,IUSER).toString()
        pass = SETTING.getString(SPASS,IPASS).toString()
        posisi = SETTING.getString(SPOS,IPOSISI).toString()
        id = SETTING.getString(SID,IID).toString()
        login()
        LAdmin.setOnClickListener(this)
        LGuru.setOnClickListener(this)
        LOrtu.setOnClickListener(this)
        LSiswa.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.LAdmin->{
                val ILogin = Intent(this,Login::class.java)
                ILogin.putExtra("pos","Admin")
                startActivityForResult(ILogin,RC_LOGIN_SUKSES)
            }
            R.id.LGuru->{
                val ILogin = Intent(this,Login::class.java)
                ILogin.putExtra("pos","Guru")
                startActivityForResult(ILogin,RC_LOGIN_SUKSES)
            }
            R.id.LOrtu->{
                val ILogin = Intent(this,Login::class.java)
                ILogin.putExtra("pos","Ortu")
                startActivityForResult(ILogin,RC_LOGIN_SUKSES)
            }
            R.id.LSiswa->{
                val ILogin = Intent(this,Login::class.java)
                ILogin.putExtra("pos","Siswa")
                startActivityForResult(ILogin,RC_LOGIN_SUKSES)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == RC_LOGIN_SUKSES){
                user=data?.extras?.getString("A").toString()
                pass=data?.extras?.getString("B").toString()
                posisi=data?.extras?.getString("C").toString()
                id=data?.extras?.getString("D").toString()
                simpan()
                login()
                //Toast.makeText(this,"Masuk user $user kedalam $posisi, $user, $pass",Toast.LENGTH_SHORT).show()
            }
            if (requestCode==RC_LOGOUT_SUKSES){
                id=""
                user=""
                pass=""
                posisi=""
                simpan()
            }
        }
    }

    fun simpan(){
        SETTING = getSharedPreferences(SNAMA,Context.MODE_PRIVATE)
        val SEDIT = SETTING.edit()
        SEDIT.putString(SUSER,user)
        SEDIT.putString(SPASS,pass)
        SEDIT.putString(SPOS,posisi)
        SEDIT.putString(SID,id)
        SEDIT.commit()
    }
    /*fun pos(){
        if(posisi=="Admin"){
            IDAdmin()
        }
        if(posisi=="Guru"){
            IDGuru()
        }
        if(posisi=="Ortu"){
            IDOrtu()
        }
        if(posisi=="Siswa"){
            IDSiswa()
        }
    }*/
    fun login(){
        if (id!=="0"&&id!==""){
            if (posisi=="Admin"){
                //Toast.makeText(this,"mulai login sebagai $posisi",Toast.LENGTH_SHORT).show()
                IDAdmin()
                //pos()
            }
            if (posisi=="Guru"){
                //Toast.makeText(this,"mulai login sebagai $posisi",Toast.LENGTH_SHORT).show()
                IDGuru()
                //pos()
            }
            if (posisi=="Ortu"){
                //Toast.makeText(this,"mulai login sebagai $posisi",Toast.LENGTH_SHORT).show()
                IDOrtu()
                //pos()
            }
            if (posisi=="Siswa"){
                //Toast.makeText(this,"mulai login sebagai $posisi",Toast.LENGTH_SHORT).show()
                IDSiswa()
                //pos()
            }
        }
        else if (id=="0"){
            //pos()
        }
    }
    fun IDAdmin(){
        val AkunAdmin = Intent(this,Admin::class.java)
        AkunAdmin.putExtra("id",id)
        startActivityForResult(AkunAdmin,RC_LOGOUT_SUKSES)
    }
    fun IDGuru(){
        val AkunGuru = Intent(this,Guru::class.java)
        AkunGuru.putExtra("id",id)
        startActivityForResult(AkunGuru,RC_LOGOUT_SUKSES)
    }
    fun IDOrtu(){
        val AkunOrtu = Intent(this,Ortu::class.java)
        AkunOrtu.putExtra("id",id)
        startActivityForResult(AkunOrtu,RC_LOGOUT_SUKSES)
    }
    fun IDSiswa(){
        val AkunSiswa = Intent(this, Siswa::class.java)
        AkunSiswa.putExtra("id",id)
        startActivityForResult(AkunSiswa,RC_LOGOUT_SUKSES)
    }
}
