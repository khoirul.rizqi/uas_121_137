package rizqi.khoirul.uas_121_137.Guru

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_guru.*
import rizqi.khoirul.uas_121_137.R
import java.util.*

class Guru : AppCompatActivity(), View.OnClickListener {
    lateinit var SETTING : SharedPreferences
    var bulan = 0
    var hari = 0
    var tahun = 0
    val SNAMA = "Setting"
    val SID = "Id"
    var a = ""
    var id = ""
    var IID = id
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guru)
        SETTING = getSharedPreferences(SNAMA, Context.MODE_PRIVATE)
        id = SETTING.getString(SID,IID).toString()
        var p : Bundle?=intent.extras
        var id = p?.getString("id").toString()
        simpan()
        val cal : Calendar = Calendar.getInstance()
        hari = cal.get(Calendar.DAY_OF_MONTH)
        bulan = cal.get(Calendar.MONTH)+1
        tahun = cal.get(Calendar.YEAR)
        a = "$tahun-$bulan-$hari"
        GProfile.setOnClickListener(this)
        GAbsen.setOnClickListener(this)
        GAbsenCek.setOnClickListener(this)
        btnLogGuru.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.GProfile->{

            }
            R.id.GAbsen->{
                val intent = Intent(this,guru_absensi::class.java)
                intent.putExtra("tanggal",a)
                startActivity(intent)
            }
            R.id.GAbsenCek->{
                val intent = Intent(this,Guru_Absen::class.java)
                startActivity(intent)
            }
            R.id.btnLogGuru->{
                finish()
            }
        }
    }

    override fun finish() {
        setResult(Activity.RESULT_OK)
        super.finish()
    }

    fun simpan(){
        SETTING = getSharedPreferences(SNAMA,Context.MODE_PRIVATE)
        val SEDIT = SETTING.edit()
        SEDIT.putString(SID,id)
        SEDIT.commit()
    }
}
