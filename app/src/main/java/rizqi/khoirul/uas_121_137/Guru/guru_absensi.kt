package rizqi.khoirul.uas_121_137.Guru

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_guru_absensi.*
import rizqi.khoirul.uas_121_137.R


class guru_absensi : AppCompatActivity(),View.OnClickListener {
    var tanggal = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guru_absensi)
        var p : Bundle?=intent.extras
        tanggal = p?.getString("tanggal").toString()
        Toast.makeText(this,tanggal,Toast.LENGTH_SHORT).show()
        btnGKAbsensi.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnGKAbsensi->{
                finish()
            }
        }
    }

    override fun finish() {
        super.finish()
    }

    override fun onStart() {
        super.onStart()
        barcode()
    }

    fun barcode(){
        val barCodeEncoder = BarcodeEncoder()
        val bitmap = barCodeEncoder.encodeBitmap(tanggal,BarcodeFormat.QR_CODE,400,400)
        Barcode.setImageBitmap(bitmap)
    }
}