package rizqi.khoirul.uas_121_137.Siswa

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_ortu_absen.*
import kotlinx.android.synthetic.main.activity_siswa_absen.*
import org.json.JSONArray
import rizqi.khoirul.uas_121_137.Ortu.AdapterDataAbsen
import rizqi.khoirul.uas_121_137.R
import java.util.*
import kotlin.collections.HashMap

class Siswa_Absensi : AppCompatActivity(), View.OnClickListener {
    lateinit var oaadapter : AdapterDataAbsen
    var daftarabseno = mutableListOf<HashMap<String,String>>()
    val url ="http://192.168.1.20/web/Android/200518_UAS/show/ortu/hadir.php"
    var id = ""
    var a = ""
    var bulan = 0
    var hari = 0
    var tahun = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_siswa_absen)
        var p : Bundle?=intent.extras
        id = p?.getString("id").toString()
        val cal : Calendar = Calendar.getInstance()
        hari = cal.get(Calendar.DAY_OF_MONTH)
        bulan = cal.get(Calendar.MONTH)+1
        tahun = cal.get(Calendar.YEAR)
        a = "$tahun-$bulan-$hari"
        oaadapter = AdapterDataAbsen(daftarabseno)
        RSAbsen.layoutManager = LinearLayoutManager(this)
        RSAbsen.adapter = oaadapter
        btnSKAbsen.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSKAbsen->{
                finish()
            }
        }
    }

    override fun finish() {
        super.finish()
    }

    override fun onStart() {
        super.onStart()
        showdataabsen("select")
    }

    fun showdataabsen(mode : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarabseno.clear()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("namasiswa",jsonObject.getString("namasiswa"))
                    mhs.put("keterangan",jsonObject.getString("keterangan"))
                    mhs.put("tanggal",jsonObject.getString("tanggal"))
                    daftarabseno.add(mhs)
                }
                oaadapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()

                when(mode){
                    "select"->{
                        hm.put("XIC",id)
                        hm.put("XIB",a)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}