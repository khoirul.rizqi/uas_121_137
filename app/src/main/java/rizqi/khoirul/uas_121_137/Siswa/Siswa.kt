package rizqi.khoirul.uas_121_137.Siswa

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_siswa.*
import org.json.JSONObject
import rizqi.khoirul.uas_121_137.R
import java.util.*

class Siswa : AppCompatActivity(), View.OnClickListener {
    val url = "http://192.168.1.20/web/Android/200518_UAS/insert/crud_siswa.php"
    lateinit var SETTING : SharedPreferences
    lateinit var intentIntegrator: IntentIntegrator
    val SNAMA = "Setting"
    val SID = "Id"
    var id = ""
    var scancode = ""
    var IID = id
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_siswa)
        SETTING = getSharedPreferences(SNAMA, Context.MODE_PRIVATE)
        id = SETTING.getString(SID,IID).toString()
        //id = "1"
        var p : Bundle?=intent.extras
        id = p?.getString("id").toString()
        simpan()
        intentIntegrator = IntentIntegrator(this)
        SProfile.setOnClickListener(this)
        SAbsen.setOnClickListener(this)
        SDataAbsen.setOnClickListener(this)
        btnLogSiswa.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.SProfile->{

            }
            R.id.SAbsen->{
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.SDataAbsen->{
                val absensi = Intent(this,Siswa_Absensi::class.java)
                absensi.putExtra("id",id)
                startActivity(absensi)
            }
            R.id.btnLogSiswa->{
                finish()
            }
        }
    }

    override fun finish() {
        setResult(Activity.RESULT_OK)
        super.finish()
    }

    fun simpan(){
        SETTING = getSharedPreferences(SNAMA,Context.MODE_PRIVATE)
        val SEDIT = SETTING.edit()
        SEDIT.putString(SID,id)
        SEDIT.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val IntentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if (IntentResult!=null){
            if (IntentResult.contents!=null){
                scancode = IntentResult.contents
                crud("insert")
            }else{
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun crud(mode : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error= jsonObject.getString("kode")
                if (error.equals("000")){
                }
                else{
                }
            },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("tgl",scancode)
                        hm.put("id",id)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
