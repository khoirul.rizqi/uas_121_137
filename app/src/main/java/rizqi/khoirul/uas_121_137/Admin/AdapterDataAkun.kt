package rizqi.khoirul.uas_121_137.Admin

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_admin_akun.*
import rizqi.khoirul.uas_121_137.R

class AdapterDataAkun(val dataAkun : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataAkun.HolderDataAkun>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataAkun.HolderDataAkun {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_admin_akun,parent,false)
        return HolderDataAkun(v)
    }

    override fun getItemCount(): Int {
        return dataAkun.size
    }

    override fun onBindViewHolder(holder: AdapterDataAkun.HolderDataAkun, position: Int) {
        val data = dataAkun.get(position)
        holder.edAkunNama.setText(data.get("username"))
        holder.edAkunUsername.setText(data.get("username"))
        holder.edAkunPassword.setText(data.get("password"))
        holder.edAkunLevel.setText(data.get("level"))
        if(position.rem(2)==0){
            holder.CAkun.setBackgroundColor(Color.rgb(230,245,240))
        }
        else{
            holder.CAkun.setBackgroundColor(Color.rgb(255,255,245))
        }
    }

    inner class HolderDataAkun(v : View): RecyclerView.ViewHolder(v){
        val edAkunNama = v.findViewById<TextView>(R.id.edAkunNama)
        val edAkunUsername = v.findViewById<TextView>(R.id.edAkunUsername)
        val edAkunPassword = v.findViewById<TextView>(R.id.edAkunPassword)
        val edAkunLevel = v.findViewById<TextView>(R.id.edAkunLevel)
        val CAkun = v.findViewById<ConstraintLayout>(R.id.CAdminAkun)
    }

}