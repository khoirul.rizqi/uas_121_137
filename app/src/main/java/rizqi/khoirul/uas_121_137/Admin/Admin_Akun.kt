package rizqi.khoirul.uas_121_137.Admin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_admin__akun.*
import kotlinx.android.synthetic.main.activity_admin__akun.btnAKAkun
import kotlinx.android.synthetic.main.activity_admin__siswa.*
import org.json.JSONArray
import rizqi.khoirul.uas_121_137.R


class Admin_Akun : AppCompatActivity(),View.OnClickListener {
    //val activity = this@Admin_Akun
    lateinit var akunAdapter : AdapterDataAkun
    var daftarAkun = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.1.20/web/Android/200518_UAS/show/data_user.php"

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            akunAdapter = AdapterDataAkun(daftarAkun)
            setContentView(R.layout.activity_admin__akun)
            RAAkun1.layoutManager = LinearLayoutManager(this)
            RAAkun1.adapter = akunAdapter
            btnAKAkun.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when(v?.id){
                R.id.btnAKAkun->{
                    finish()
                }
            }
        }

        fun showDataAkun(){
            val request = object : StringRequest(Request.Method.POST,url,
                Response.Listener { response ->
                    daftarAkun.clear()
                    val jsonArray = JSONArray(response)
                    for(x in 0..(jsonArray.length()-1)){
                        val jsonObject = jsonArray.getJSONObject(x)
                        var mhs = HashMap<String,String>()
                        mhs.put("username",jsonObject.getString("username"))
                        mhs.put("password",jsonObject.getString("password"))
                        mhs.put("level",jsonObject.getString("level"))
                        daftarAkun.add(mhs)
                    }
                    akunAdapter.notifyDataSetChanged()
                },
                Response.ErrorListener { error ->
                }){
            }
            val queue = Volley.newRequestQueue(this)
            queue.add(request)
        }

        override fun finish() {
            super.finish()
        }

        override fun onStart() {
            super.onStart()
            showDataAkun()
        }

    }

