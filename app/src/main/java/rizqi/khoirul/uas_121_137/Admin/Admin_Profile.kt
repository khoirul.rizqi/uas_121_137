package rizqi.khoirul.uas_121_137.Admin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_admin__profile.*
import rizqi.khoirul.uas_121_137.R

class Admin_Profile : AppCompatActivity(), View.OnClickListener {
    var id = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin__profile)
        var p : Bundle?=intent.extras
        var id = p?.getString("id").toString()
        edAId.setText(id)
        btnAKeluar.setOnClickListener(this)
        btnASimpan.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnAKeluar->{
                finish()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        showdata()
    }

    fun showdata(){
        
    }
}
