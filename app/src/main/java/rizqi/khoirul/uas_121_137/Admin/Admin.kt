package rizqi.khoirul.uas_121_137.Admin

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_admin.*
import rizqi.khoirul.uas_121_137.R

class Admin : AppCompatActivity(), View.OnClickListener {
    lateinit var SETTING : SharedPreferences
    val SNAMA = "Setting"
    val SID = "Id"
    var id = ""
    var IID = id
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        SETTING = getSharedPreferences(SNAMA, Context.MODE_PRIVATE)
        id = SETTING.getString(SID,IID).toString()
        var p : Bundle?=intent.extras
        var id = p?.getString("id").toString()
        simpan()
        AProfile.setOnClickListener(this)
        AAkun.setOnClickListener(this)
        AIjin.setOnClickListener(this)
        AReg.setOnClickListener(this)
        ASiswa.setOnClickListener(this)
        btnLogAdmin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.AProfile->{
                val AdProfile = Intent(this,Admin_Profile::class.java)
                AdProfile.putExtra("id",id)
                startActivity(AdProfile)
            }
            R.id.AAkun->{
                val AdAkun = Intent(this,Admin_Akun::class.java)
                startActivity(AdAkun)
            }
            R.id.AIjin->{
                val AdIjin = Intent(this,Admin_Ijin::class.java)
                startActivity(AdIjin)
            }
            R.id.ASiswa->{
                val AdSiswa = Intent(this,Admin_Siswa::class.java)
                startActivity(AdSiswa)
            }
            R.id.btnLogAdmin->{
                finish()
            }
        }
    }

    override fun finish() {
        setResult(Activity.RESULT_OK)
        super.finish()
    }

    fun simpan(){
        SETTING = getSharedPreferences(SNAMA,Context.MODE_PRIVATE)
        val SEDIT = SETTING.edit()
        SEDIT.putString(SID,id)
        SEDIT.commit()
    }
}
