package rizqi.khoirul.uas_121_137.Admin

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import rizqi.khoirul.uas_121_137.R

class AdapterAdminAbsen(val dataAbsen : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterAdminAbsen.HolderAdminAbsen>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterAdminAbsen.HolderAdminAbsen {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_data_absen,parent,false)
        return HolderAdminAbsen(v)
    }

    override fun getItemCount(): Int {
        return dataAbsen.size
    }

    override fun onBindViewHolder(holder: AdapterAdminAbsen.HolderAdminAbsen, position: Int) {
        val data = dataAbsen.get(position)
        holder.edNama.setText(data.get("namasiswa"))
        holder.edKelas.setText(data.get("kelasid"))
        holder.edKeterangan.setText(data.get("keterangan"))
        holder.edtanggal.setText(data.get("tanggal"))
        if(position.rem(2)==0){
            holder.CAbsen.setBackgroundColor(Color.rgb(230,245,240))
        }
        else{
            holder.CAbsen.setBackgroundColor(Color.rgb(255,255,245))
        }
    }

    inner class HolderAdminAbsen(v : View): RecyclerView.ViewHolder(v) {
        val edNama = v.findViewById<TextView>(R.id.txabsennama)
        val edKelas = v.findViewById<TextView>(R.id.txabsenkelas)
        val edKeterangan = v.findViewById<TextView>(R.id.txabsenket)
        val edtanggal = v.findViewById<TextView>(R.id.txabsentanggal)
        val CAbsen = v.findViewById<ConstraintLayout>(R.id.Cabsensi)
    }
}