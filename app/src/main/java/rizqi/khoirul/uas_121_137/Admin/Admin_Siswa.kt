package rizqi.khoirul.uas_121_137.Admin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_admin__siswa.*
import org.json.JSONArray
import rizqi.khoirul.uas_121_137.Ortu.AdapterDataAbsen
import rizqi.khoirul.uas_121_137.R
import java.util.*
import kotlin.collections.HashMap

class Admin_Siswa : AppCompatActivity(),View.OnClickListener {

    lateinit var oaadapter : AdapterAdminAbsen
    var daftarabseno = mutableListOf<HashMap<String,String>>()
    val url ="http://192.168.1.20/web/Android/200518_UAS/show/admin/hadir.php"
    var id = ""
    var a = ""
    var bulan = 0
    var hari = 0
    var tahun = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin__siswa)
        val cal : Calendar = Calendar.getInstance()
        hari = cal.get(Calendar.DAY_OF_MONTH)
        bulan = cal.get(Calendar.MONTH)+1
        tahun = cal.get(Calendar.YEAR)
        a = "$tahun-$bulan-$hari"
        oaadapter = AdapterAdminAbsen(daftarabseno)
        RASiswa.layoutManager = LinearLayoutManager(this)
        RASiswa.adapter = oaadapter
        btnAKAkun.setOnClickListener(this)
    }

    override fun finish() {
        super.finish()
    }

    override fun onStart() {
        super.onStart()
        showdataabsen()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnAKAkun->{
                finish()
            }
        }
    }

    fun showdataabsen(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarabseno.clear()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("namasiswa",jsonObject.getString("namasiswa"))
                    mhs.put("kelasid",jsonObject.getString("kelasid"))
                    mhs.put("keterangan",jsonObject.getString("keterangan"))
                    mhs.put("tanggal",jsonObject.getString("tanggal"))
                    daftarabseno.add(mhs)
                }
                oaadapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
            }){
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
