package rizqi.khoirul.uas_121_137.Ortu

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import rizqi.khoirul.uas_121_137.R

class AdapterDataAbsen(val dataAbsen : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataAbsen.HolderDataAbsen>()  {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataAbsen.HolderDataAbsen {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_absensi,parent,false)
        return HolderDataAbsen(v)
    }

    override fun getItemCount(): Int {
        return dataAbsen.size
    }

    override fun onBindViewHolder(holder: AdapterDataAbsen.HolderDataAbsen, position: Int) {
        val data = dataAbsen.get(position)
        holder.edNama.setText(data.get("namasiswa"))
        holder.edKeterangan.setText(data.get("keterangan"))
        holder.edtanggal.setText(data.get("tanggal"))
        if(position.rem(2)==0){
            holder.CAbsen.setBackgroundColor(Color.rgb(230,245,240))
        }
        else{
            holder.CAbsen.setBackgroundColor(Color.rgb(255,255,245))
        }
    }

    inner class HolderDataAbsen(v : View): RecyclerView.ViewHolder(v){
        val edNama = v.findViewById<TextView>(R.id.txabsennama2)
        val edKeterangan = v.findViewById<TextView>(R.id.txabsenket2)
        val edtanggal = v.findViewById<TextView>(R.id.txabsentanggal2)
        val CAbsen = v.findViewById<ConstraintLayout>(R.id.CAbsenAbsen)
    }
}