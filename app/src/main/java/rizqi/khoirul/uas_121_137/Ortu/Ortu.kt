package rizqi.khoirul.uas_121_137.Ortu

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_ortu.*
import org.json.JSONObject
import rizqi.khoirul.uas_121_137.R
import java.util.*
import kotlin.collections.HashMap

class Ortu : AppCompatActivity(),View.OnClickListener {
    lateinit var SETTING : SharedPreferences
    val SNAMA = "Setting"
    val SID = "Id"
    var id = ""
    var IID = id
    val url = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ortu)
        SETTING = getSharedPreferences(SNAMA, Context.MODE_PRIVATE)
        id = SETTING.getString(SID,IID).toString()
        var p : Bundle?=intent.extras
        id = p?.getString("id").toString()
        simpan()
        OProfile.setOnClickListener(this)
        OIjin.setOnClickListener(this)
        ODaftarAbsen.setOnClickListener(this)
        btnLogOrtu.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.OProfile->{

            }
            R.id.ODaftarAbsen->{
                val absensi = Intent(this,Ortu_Absen::class.java)
                absensi.putExtra("id",id)
                startActivity(absensi)
            }
            R.id.OIjin->{

            }
            R.id.btnLogOrtu->{
                finish()
            }
        }
    }

    override fun finish() {
        setResult(Activity.RESULT_OK)
        super.finish()
    }

    fun simpan(){
        SETTING = getSharedPreferences(SNAMA,Context.MODE_PRIVATE)
        val SEDIT = SETTING.edit()
        SEDIT.putString(SID,id)
        SEDIT.commit()
    }


}
